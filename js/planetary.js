(function(undefined){

	var $container = $("#holder");
	var $paperElement = $(".paper", $container);

	var options = {
		line: {
			fill: "#f00",
			stroke: "#00f"
		},
		canvas: {
			x: 0,
			y: 0,
			width: 1024,
			height: 1024
		},
		graphics: {
			sun: {
				drawOptions: {
					fill: "yellow",
					stroke: "red",
				}
			},
			planets: {
				drawOptions: {
					fill: "green",
					stroke: "blue"
				}
			},
			moons: {
				drawOptions: {
					fill: "grey",
					stroke: "white",
					r: 6
				}
			},
			orbits: {
				stroke: "#ccc"
			},
			legend: {
				cssOptions: {
					"width": "50%",
					"margin": "0px auto",
					"background-color": "#ccc"
				}
			}
		},
		planets: {
			moho: {
				semimajor: 5263138304,
				apoapsis: 6315765980,
				periapsis: 4210510628,
				orbitalperiod: 2215754.2,
				orbitalvelocity: 12186.1,
				radius: 250000,
				parentObject: "kerbol",
				eccentricity: 0.2,
				gravParameter: 24525e7,
				longAscNode: 0,
				inclination: 7,
				meanAnomaly: 3.16
			},
			eve: {
				semimajor: 9832684544,
				apoapsis: 9931011387,
				periapsis: 9734357701,
				orbitalperiod: 5658120,
				orbitalvelocity: 10810.5,
				radius: 700000,
				parentObject: "kerbol",
				eccentricity: 0.01,
				gravParameter: 81717302e5,
				longAscNode: 0,
				inclination: 2.1,
				meanAnomaly: 3.15,
				moons: {
					gilly: {
						semimajor: 31500000,
						apoapsis: 48825000,
						periapsis: 14175000,
						orbitalperiod: 388440,
						orbitalvelocity: 515.6,
						radius: 13000,
						parentObject: "eve",
						eccentricity: 0.55,
						gravParameter: 8289449.8,
						longAscNode: 0,
						inclination: 12,
						meanAnomaly: 1
					}
				}
			},
			kerbin: {
				semimajor: 13338840256,
				apoapsis: 13338840256,
				periapsis: 13338840256,
				orbitalperiod: 9203544.6,
				orbitalvelocity: 9284.5,
				radius: 600000,
				parentObject: "kerbol",
				eccentricity: 0,
				gravParameter: 35316e8,
				longAscNode: 0,
				inclination: 0,
				meanAnomaly: 3.14,
				moons: {
					mun: {
						semimajor: 12000000,
						apoapsis: 12000000,
						periapsis: 12000000,
						orbitalperiod: 138984.38,
						orbitalvelocity: 542.5,
						radius: 200000,
						parentObject: "kerbin",
						eccentricity: 0,
						gravParameter: 65138398e3,
						longAscNode: 0,
						inclination: 0,
						meanAnomaly: 1.98
					},
					minmus: {
						semimajor: 47000000,
						apoapsis: 47000000,
						periapsis: 47000000,
						orbitalperiod: 1077310.5,
						orbitalvelocity: 274.1,
						radius: 60000,
						parentObject: "kerbin",
						eccentricity: 0,
						gravParameter: 17658e5,
						longAscNode: 0,
						inclination: 6,
						meanAnomaly: 0.94
					}
				}
			},
			duna: {
				semimajor: 20726155264,
				apoapsis: 21783189163,
				periapsis: 19669121365,
				orbitalperiod: 17315400,
				orbitalvelocity: 7146.6,
				radius: 320000,
				parentObject: "kerbol",
				eccentricity: 0.05,
				gravParameter: 30136321e4,
				longAscNode: 0,
				inclination: 0.06,
				meanAnomaly: 3.14,
				moons: {
					ike: {
						semimajor: 3200000,
						apoapsis: 3296000,
						periapsis: 3104000,
						orbitalperiod: 65517.862,
						orbitalvelocity: 334.8,
						radius: 130000,
						parentObject: "duna",
						eccentricity: 0.03,
						gravParameter: 18568369e3,
						longAscNode: 0,
						inclination: 0.2,
						meanAnomaly: 3.14
					}
				}
			},
			dres: {
				semimajor: 40839348203,
				apoapsis: 46761053522,
				periapsis: 34917642884,
				orbitalperiod: 47893063,
				orbitalvelocity: 0,
				radius: 138000,
				parentObject: "kerbol",
				eccentricity: 0.14,
				gravParameter: 21483606e3,
				longAscNode: 0,
				inclination: 5,
				meanAnomaly: 0.17
			},
			jool: {
				semimajor: 68773560320,
				apoapsis: 71950638386,
				periapsis: 65073282253,
				orbitalperiod: 104661360,
				orbitalvelocity: 3927.2,
				radius: 6000000,
				parentObject: "kerbol",
				eccentricity: 0.05,
				gravParameter: 2.82528e14,
				longAscNode: 0,
				inclination: 1.304,
				meanAnomaly: 0.1,
				moons: {
					laythe: {
						semimajor: 27184000,
						apoapsis: 27184000,
						periapsis: 27184000,
						orbitalperiod: 52980.879,
						orbitalvelocity: 3223.8,
						radius: 500000,
						parentObject: "jool",
						eccentricity: 0,
						gravParameter: 1962e9,
						longAscNode: 0,
						inclination: 0,
						meanAnomaly: 1.26
					},
					vall: {
						semimajor: 43152000,
						apoapsis: 43152000,
						periapsis: 43152000,
						orbitalperiod: 105962.09,
						orbitalvelocity: 2558.8,
						radius: 300000,
						parentObject: "jool",
						eccentricity: 0,
						gravParameter: 2074815e5,
						longAscNode: 0,
						inclination: 0,
						meanAnomaly: 1.26
					},
					tylo: {
						semimajor: 68500000,
						apoapsis: 68500000,
						periapsis: 68500000,
						orbitalperiod: 211926.36,
						orbitalvelocity: 2030.9,
						radius: 600000,
						parentObject: "jool",
						eccentricity: 0,
						gravParameter: 282528e7,
						longAscNode: 0,
						inclination: 0.025,
						meanAnomaly: 3.32
					},
					bop: {
						semimajor: 104500000,
						apoapsis: 129057500,
						periapsis: 79942500,
						orbitalperiod: 399322.29,
						orbitalvelocity: 1784.3,
						radius: 65000,
						parentObject: "jool",
						eccentricity: 0.235,
						gravParameter: 24868349e2,
						longAscNode: 0,
						inclination: 15,
						meanAnomaly: 1
					},
					pol: {
						semimajor: 3200000,
						apoapsis: 152081706,
						periapsis: 129890000,
						orbitalperiod: 553341,
						orbitalvelocity: 0,
						radius: 44000,
						parentObject: "jool",
						eccentricity: 0.17,
						gravParameter: 2275834e2,
						longAscNode: 0,
						inclination: 4.25,
						meanAnomaly: 4.85
					}
				}
			}
		},
		suns: {
			kerbol: {
				radius: 261600000,
				x: 512,
				y: 512,
				gravParameter: 1.1723328e18
			}
		}
	}

	var time = 1;
	zoomBase = 1e-9;
	var zoom = zoomBase;
	var zoomF = 40;
	var parentPos = {x: 0, y: 0, z: 0};
	var stepTime = 100;
	var running = false;
	var timer;
	var objectcache = {};

	var call = function() {
		paper = Raphael($paperElement, 0, 50, 512, 512);
		paper.setSize(800, 600);
		setupUniverse();
	}

	var setupUniverse = function() {
		parentPos.x = paper.width / 2;
		parentPos.y = paper.height / 2;
		var sun = paper.circle(parentPos.x , parentPos.y, options.suns.kerbol.radius * zoom + 4);
		sun.attr(options.graphics.sun.drawOptions);
		addUniverseObjects();
	};

	var addUniverseObjects = function() {
		$.each(options.planets, function(i,v) {
			var that = this;
			if (objectcache[i] === undefined) {
				that.parentPos = parentPos;
				that.name = i;
				that.planetPos = placeCelestialBody(that);
				var planet = paper.circle(that.planetPos.x, that.planetPos.y, that.radius * zoom + 3);
				planet.attr(options.graphics.planets.drawOptions);
				planet.id = i;
				that.time = time;
				if (v.moons) {
					$.each(v.moons, function(j,k) {
						this.parentPos = that.planetPos;
						this.name = j;
						this.planetPos = placeCelestialBody(this);
						this.moonSize = options.graphics.moons.drawOptions.r;
						var moon = paper.rect(this.planetPos.x - (this.moonSize / 2), this.planetPos.y - (this.moonSize / 2), this.moonSize , this.moonSize);
						moon.attr(options.graphics.moons.drawOptions);
						moon.id = j;
						this.time = time;
						this.isMoon = true;
						that.moons[j] = this;
					});
				}
			}
			objectcache[i] = that;
		});
	}

	var animateObjects = function() {
		this.objectCache = objectcache;
		$.each(this.objectCache, function (i,v) {
			var that = this;
			var element = paper.getById(i);
			that.time = time;
			var newPlanetPos = placeCelestialBody(that);
			element.animate({cx: newPlanetPos.x, cy: newPlanetPos.y}, stepTime, "linear");
			that.planetPos = newPlanetPos;
			if (objectcache[i]["moons"] !== undefined) {
				$.each(that.moons, function(j,k) {
					var moonElement = paper.getById(j);
					this.parentPos = that.planetPos;
					this.time = time;
					var newMoonPos = placeCelestialBody(this);
					moonElement.animate({x: newMoonPos.x - 3, y: newMoonPos.y - 3}, stepTime, "linear");
					this.planetPos = newMoonPos;
					that.moons[j] = this;
				});
			}
			objectcache[i] = that;
		});
	}

	var placeCelestialBody = function(object) {
		if (!object.semiminor) {
			var focalDist = object.semimajor * object.eccentricity;
			object.semiminor = Math.sqrt(((focalDist - object.semimajor) * -1) * (object.semimajor - focalDist));
			object.focus = focalDist;
			object.avgorbitalvelocity = (2 * Math.PI * object.semimajor) / object.orbitalperiod;
		}
		drawOrbits(object);
		var vec = {x: object.parentPos.x + object.focus * zoom, y: object.parentPos.y , z: 0};
		var calcX = Math.sin((object.avgorbitalvelocity / stepTime) * time / 1000) * (object.semimajor * zoom);
		var calcY = Math.cos((object.avgorbitalvelocity / stepTime) * time / 1000) * (object.semiminor * zoom);
		vec.x += calcX;
		vec.y += calcY;
		vec.z = 0;
		var curXOffset = object.parentPos.x - calcX;
		var curYOffset = object.parentPos.x - calcY;
// 		console.log(curXOffset, curYOffset);
		return vec;
	}

	var drawOrbits = function(object) {
		if (paper.getById("orbit-"+object.name)) {
			var orbit = paper.getById("orbit-"+object.name);
			if (object.planetPos !== undefined) {
				if (this.isMoon) {
					orbit.animate({cx: object.planetPos.x, cy: object.planetPos.y}, stepTime, "linear");
				}
			}
		} else {
			var orbit = paper.ellipse(object.parentPos.x, object.parentPos.y, object.semimajor * zoom, object.semiminor * zoom);
		}
		orbit.attr(options.graphics.orbits);
		orbit.id = "orbit-"+object.name;
	};

	var adjustOrbits = function() {
		$.each(objectcache, function(i,v) {
			curOrbit = paper.getById("orbit-"+v.name);
			curOrbit.animate({rx: v.semimajor * zoom, ry: v.semiminor * zoom, cx: v.parentPos.x + v.focus * zoom}, stepTime, "linear");
		});
	}

	function drawConnector(origin, target) {
		var line = paper.path("M"+origin.x+","+origin.y+" "+target.x+","+target.y);
		line.attr(options.graphics.orbits);
	}

	function runTimer() {
		if (running == false) {
			console.log("Start the clock");
			running = true;
			timer=setInterval(function(){
				time++;
				animateObjects();
			},stepTime);
		}
	}

	function stopTimer() {
		if (running == true) {
			console.log("Stop the clock");
			running = false;
			clearInterval(timer);
		}
		console.log(objectcache);
	}

	function changeZoomLevel(zoomF) {
		zoom = zoomBase * zoomF / 10;
		animateObjects();
		adjustOrbits();
	}

	$(document).ready(function() {
		$("input#start").bind("click", function(event) {
			runTimer();
		});

		$("input#stop").bind("click", function(event) {
			stopTimer();
		});

		$(".legend").bind("mousewheel", function(event) {
			event.preventDefault();
			if (event.originalEvent.wheelDelta / 120 < 0) {
				if (zoomF <= 1000) {
					zoomF /= 1.1;
				} else {
					zoomF = 1000;
				}
				changeZoomLevel(zoomF);
			}
			if (event.originalEvent.wheelDelta / 120 > 0) {
				if (zoomF >= 0.1) {
					zoomF *= 1.1;
				} else {
					zoomF = 0.1;
				}
				changeZoomLevel(zoomF);
			}
			$("#slider-vertical").slider("option", "value", zoomF);
		});
		$("#slider-vertical").slider({
			orientation: "vertical",
			range: "min",
			min: 1,
			max: 1000,
			value: 10,
			slide: function( event, ui ) {
				changeZoomLevel(ui.value);
			}
		});
		call();
	});
})();
